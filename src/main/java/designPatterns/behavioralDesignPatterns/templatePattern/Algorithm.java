package designPatterns.behavioralDesignPatterns.templatePattern;

/**
 * Created by ahiticas on
 * 10/31/2017.
 */

public abstract class Algorithm {

    public abstract void initialize();
    public abstract void sorting();
    public abstract void printResult();



    public void sort() {
        initialize();
        sorting();
        printResult();
    }
}
