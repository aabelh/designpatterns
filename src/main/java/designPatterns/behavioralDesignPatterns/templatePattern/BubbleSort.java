package designPatterns.behavioralDesignPatterns.templatePattern;

/**
 * Created by ahiticas on
 * 10/31/2017.
 */

public class BubbleSort extends Algorithm {


    private int[] numbers;

    public BubbleSort(int[] numbers) {
        this.numbers = numbers;
    }

    @Override
    public void initialize() {
        System.out.println("Initializing bubble sort...");
    }

    @Override
    public void sorting() {

        int n = this.numbers.length;
        int tmp;

        while (n != 0) {
            tmp = 0;

            for (int i=1; i < n; i++) {
                if (numbers[i-1] > numbers[i]) {
                    int swqap = numbers[i-1];
                    numbers[i-1] =  numbers[i];
                    numbers[i] = swqap;
                    tmp =i;
                }
            }
            n = tmp;
        }

    }

    @Override
    public void printResult() {
        for (Integer in: numbers) {
            System.out.print(in + " ");
        }
    }
}
