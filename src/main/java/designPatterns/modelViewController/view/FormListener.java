package designPatterns.modelViewController.view;

public interface FormListener {

    void okButtonClicked(String personName, String personOccupation);
}
