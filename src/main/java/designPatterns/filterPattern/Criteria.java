package designPatterns.filterPattern;

import java.util.List;

public interface Criteria {

      List<Person> meetCriteria(List<Person> list);
}
