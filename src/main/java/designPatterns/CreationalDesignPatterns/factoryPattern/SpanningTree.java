package designPatterns.CreationalDesignPatterns.factoryPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class SpanningTree implements Algorithm {
    @Override
    public void solve() {
        System.out.println("solving the spanning tree problem..");
    }
}
