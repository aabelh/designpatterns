package designPatterns.behavioralDesignPatterns.iteratorPattern;

/**
 * Created by ahiticas on
 * 10/31/2017.
 */

public interface Iterator {

    public boolean hasNext();

    public Object next();
}
