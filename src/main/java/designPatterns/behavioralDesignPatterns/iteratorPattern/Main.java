package designPatterns.behavioralDesignPatterns.iteratorPattern;

import com.sun.org.apache.xpath.internal.operations.String;

/**
 * Created by ahiticas on
 * 10/31/2017.
 */

public class Main {

    /*
            Iterator pattern is very commonly used design pattern in Java!!
                This is used to get a way to access the elements of a collection object in
                    sequential manner without any need to know its underlying representation
     */

    public static void main(String[] args) {

        NameRepository nameRepository = new NameRepository();
        for (Iterator it = nameRepository.getIterator(); it.hasNext(); ) {
            String name = (String) it.next();
            System.out.println(name);
        }

    }
}
