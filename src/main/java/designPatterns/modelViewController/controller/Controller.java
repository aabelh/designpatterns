package designPatterns.modelViewController.controller;

import designPatterns.modelViewController.model.Database;
import designPatterns.modelViewController.model.Person;
import designPatterns.modelViewController.view.MainFrame;

import java.util.List;

public class Controller {
    private Database database;
    private MainFrame mainFrame;

    public Controller(MainFrame mainFrame) {
        this.database = new Database();
        this.mainFrame = mainFrame;
    }

    public void addPerson(String pName, String pOccupation){
        Person p = new Person(pName, pOccupation);
        this.database.addPerson(p);
    }

    public void removePerson(Person p){
        this.database.removePerson(p);
    }

    public List<Person> getPeopleDatabase(){
        return this.database.getPersonList();
    }

    public void refreshScreen(){
        List<Person> people = getPeopleDatabase();
        this.mainFrame.clearText();

        for(Person p: people){
            this.mainFrame.refreshTextArea(p.getName(), p.getOccupation());
        }
    }

}
