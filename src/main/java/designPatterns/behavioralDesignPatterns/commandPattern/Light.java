package designPatterns.behavioralDesignPatterns.commandPattern;

/**
 * Created by ahiticas on
 * 10/20/2017.
 */

//receiver
public class Light {

    public void turnOn(){
        System.out.println("Lights are on...");
    }

    public void turnOff() {
        System.out.println("Lights are off...");
    }

}
