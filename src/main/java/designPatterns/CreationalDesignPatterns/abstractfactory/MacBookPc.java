package designPatterns.CreationalDesignPatterns.abstractfactory;

public class MacBookPc implements Computer {
    @Override
    public String getRam() {
        return "16GB";
    }

    @Override
    public String getManufacturer() {
        return "MacBookPc";
    }
}
