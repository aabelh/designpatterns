package designPatterns.behavioralDesignPatterns.nullObjectPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class RealCustomer extends AbstractCustomer {


    private String customerName;
    public RealCustomer(String customerName) {
        this.customerName = customerName;
    }

    public boolean isNull() {
        return false;
    }

    public String getCustomer() {
        return this.customerName;
    }
}
