package designPatterns.modelViewController.model;

import java.util.ArrayList;
import java.util.List;

public class Database {

    private List<Person> personList;

    public Database() {
        personList = new ArrayList<>();
    }

    public void addPerson(Person p){
        this.personList.add(p);
    }

    public void removePerson(Person person){
        this.personList.remove(person);
    }

    public List<Person> getPersonList() {
        return personList;
    }
}
