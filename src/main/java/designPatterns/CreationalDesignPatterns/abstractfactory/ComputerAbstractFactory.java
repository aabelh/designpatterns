package designPatterns.CreationalDesignPatterns.abstractfactory;

public interface ComputerAbstractFactory {
    Computer create();
}
