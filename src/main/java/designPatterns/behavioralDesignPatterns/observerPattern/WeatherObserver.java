package designPatterns.behavioralDesignPatterns.observerPattern;

/**
 * Created by ahiticas on
 * 10/19/2017.
 */

public class WeatherObserver implements Observer {

    private int presure;
    private int temp;
    private int humidity;
    private Subject subject;

    public WeatherObserver(Subject subject) {
        this.subject = subject;
        this.subject.addObserver(this);
    }

    @Override
    public void update(int pressure, int temp, int humidity) {
        this.presure = pressure;
        this.temp = temp;
        this.humidity = humidity;

        showData();
    }

    private void showData() {
        System.out.println("Pressure: "+ presure + "  Temp: " + temp + "  Humidity: " + humidity);
    }
}
