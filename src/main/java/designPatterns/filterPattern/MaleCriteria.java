package designPatterns.filterPattern;

import java.util.ArrayList;
import java.util.List;

public class MaleCriteria implements Criteria {

    @Override
    public List<Person> meetCriteria(List<Person> list) {
       List<Person> maleCriteria = new ArrayList<Person>();

       for(Person p: list){
           if(p.getGender().equals(Person.Gender.MALE)){
               maleCriteria.add(p);
           }
       }
       return maleCriteria;
    }
}
