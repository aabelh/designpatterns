package designPatterns.structuralDesignPatterns.adapterPattern;

public interface Vehicle {

    void accelerate();
}
