package designPatterns.behavioralDesignPatterns.iteratorPattern;

/**
 * Created by ahiticas on
 * 10/31/2017.
 */

public class NameRepository {

    private String [] names = {"Adam", "Joe", "John"};


    public Iterator getIterator() {
        return new NameIterator(names);
    }

}
