package designPatterns.behavioralDesignPatterns.strategyPattern;

/**
 * Created by ahiticas on
 * 10/18/2017.
 */

public class Add implements Strategy {
    public void operation(int num1, int num2) {
        System.out.println(num1+num2);
    }
}
