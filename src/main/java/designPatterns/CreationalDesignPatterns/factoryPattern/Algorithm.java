package designPatterns.CreationalDesignPatterns.factoryPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public interface Algorithm {

    public void solve();

}
