package designPatterns.modelViewController.view;

import javax.swing.*;
import java.awt.*;

public class TextPanel extends JPanel {

    private JTextArea textArea;

    public TextPanel() {
        initClasses();
        setLayout();
    }

    private void initClasses() {
        this.textArea = new JTextArea();
    }

    private void setLayout() {
        setLayout(new BorderLayout());
        textArea.setFont(new Font("Tahoma", Font.PLAIN, 14));
        add(new JScrollPane(textArea), BorderLayout.CENTER);
    }
    public void addText(String text){
        this.textArea.append(text+"\n");
    }

    public void clearText(){
        this.textArea.setText("");
    }



}
