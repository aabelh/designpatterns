package designPatterns.behavioralDesignPatterns.visitorPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class Table implements ShoppingItem{

    private String type;
    private double price;

    public Table(String type, double price) {
        this.type = type;
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double accpet(ShoppingCartVisitor visitor) {
        return visitor.visit(this);
    }

}
