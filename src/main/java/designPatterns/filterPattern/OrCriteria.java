package designPatterns.filterPattern;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class OrCriteria implements Criteria {

    private Criteria criteria;
    private Criteria otherCriteria;

    public OrCriteria(Criteria criteria, Criteria otherCriteria) {
        this.criteria = criteria;
        this.otherCriteria = otherCriteria;
    }

    @Override
    public List<Person> meetCriteria(List<Person> list) {
        List<Person> firsCriteiaItems = criteria.meetCriteria(list);
        List<Person> otherCriteriaItems = otherCriteria.meetCriteria(list);

//        Set<Person> set = Sets.intersection(Sets.<Person>newHashSet(firsCriteiaItems), Sets.newHashSet(otherCriteriaItems));



//        for(Person p : set)
//        {
//            System.out.println(p.toString());
//        }

//        return Lists.newArrayList(set);
//        Collection col1 = new ArrayList(firsCriteiaItems);
//        Collection col2 = new ArrayList(otherCriteriaItems);


        //col1.retainAll(otherCriteriaItems);

//        for(Object c: col1){
//            System.out.println(c.toString());
//        }

//        return (List<Person>) col1;
        for(Person p: otherCriteriaItems){
            if(!firsCriteiaItems.contains(p)){
                firsCriteiaItems.add(p);
            }
        }

        return firsCriteiaItems;
//        firsCriteiaItems.addAll(otherCriteriaItems);
//        return firsCriteiaItems;
    }
}
