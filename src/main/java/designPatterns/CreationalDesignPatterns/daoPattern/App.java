package designPatterns.CreationalDesignPatterns.daoPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class App {

    /*
            DAO - DataAccessObject
     */

    public static void main(String[] args) {
        Database database = new Database();

        database.insert(new Person("Joe", 15));
        database.insert(new Person("Adam", 25));
        database.insert(new Person("John", 35));

        for(Person person: database.getPersons()) {
            System.out.println(person);
        }

        Person p = new Person("adam", 24);
        database.insert(p);
        System.out.println("\n");
        for(Person person: database.getPersons()) {
            System.out.println(person);
        }
        System.out.println("\n");

        database.remove(p);
        System.out.println("\n");
        for(Person person: database.getPersons()) {
            System.out.println(person);
        }



    }

}
