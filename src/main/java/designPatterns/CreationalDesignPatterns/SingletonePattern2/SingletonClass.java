package designPatterns.CreationalDesignPatterns.SingletonePattern2;

import com.sun.org.apache.bcel.internal.generic.INSTANCEOF;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public enum  SingletonClass {

    INSTANCE;

    private int counter;

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getCounter() {
        return this.counter;
    }
}
