package designPatterns.CreationalDesignPatterns.abstractfactory;

public class LenovoPc implements Computer {
    @Override
    public String getRam() {
        return "4GB";
    }

    @Override
    public String getManufacturer() {
        return "LenovoPC";
    }
}
