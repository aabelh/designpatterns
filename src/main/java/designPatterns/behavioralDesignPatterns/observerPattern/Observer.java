package designPatterns.behavioralDesignPatterns.observerPattern;

/**
 * Created by ahiticas on
 * 10/19/2017.
 */

public interface Observer {
    public void update(int pressure, int temp, int humidity);
}
