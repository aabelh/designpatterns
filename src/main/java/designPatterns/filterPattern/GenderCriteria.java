package designPatterns.filterPattern;

import org.omg.CORBA.PERSIST_STORE;

import java.util.ArrayList;
import java.util.List;

public class GenderCriteria implements  Criteria {

    private Person.Gender gender;

    public GenderCriteria(Person.Gender gender) {
        this.gender = gender;
    }

    @Override
    public List<Person> meetCriteria(List<Person> list) {
        List<Person> result = new ArrayList<Person>();

        for(Person p : list){
            if(p.getGender().equals(gender)){
                result.add(p);
            }
        }
        return result;
    }
}
