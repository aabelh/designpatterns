package designPatterns.CreationalDesignPatterns.abstractfactory;

public class MacBookPcFactory implements ComputerAbstractFactory {
    @Override
    public Computer create() {
        return new MacBookPc();
    }
}
