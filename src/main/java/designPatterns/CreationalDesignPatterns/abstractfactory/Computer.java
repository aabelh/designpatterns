package designPatterns.CreationalDesignPatterns.abstractfactory;

public interface Computer {
    String getRam();
    String getManufacturer();
}
