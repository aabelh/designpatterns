package designPatterns.behavioralDesignPatterns.nullObjectPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public abstract class AbstractCustomer {

    protected String personName;
    public abstract boolean isNull();
    public abstract String getCustomer();
}
