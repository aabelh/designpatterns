package designPatterns.filterPattern;

public class Person {

    public enum Gender{
        MALE,
        FEMALE
    }
    public enum MaritalStatus{
        MARRIED,
        SINGLE
    }

    private String name;
    private Gender gender;
    private MaritalStatus maritalStatus;
    private int age;

    public Person(String name, Gender gender, MaritalStatus maritalStatus, int age) {
        this.name = name;
        this.gender = gender;
        this.maritalStatus = maritalStatus;
        this.age = age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", maritalStatus=" + maritalStatus +
                ", age=" + age +
                '}';
    }
}
