package designPatterns.structuralDesignPatterns.facadePattern;

public interface Algorithm {

    void sort();

}
