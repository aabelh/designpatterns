package designPatterns.behavioralDesignPatterns.commandPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahiticas on
 * 10/20/2017.
 */

//invoker
public class Switcher {


    public List<Command> commands;

    public Switcher() {
        commands = new ArrayList<>();
    }

    public void addCommand(Command command) {
        this.commands.add(command);
    }

    public void executeCommands() {
        for(Command command : commands) {
            command.execute();
        }

    }
}
