package designPatterns.behavioralDesignPatterns.visitorPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public interface ShoppingCartVisitor {
    public double visit(Table table);
    public double visit(Chair chair);
}
