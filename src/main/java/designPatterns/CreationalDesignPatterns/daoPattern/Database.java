package designPatterns.CreationalDesignPatterns.daoPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class Database implements PersonDAO {

    private List<Person> people;

    public Database() {
        people = new ArrayList<>();
    }

    @Override
    public void insert(Person person) {
        this.people.add(person);
    }

    @Override
    public void remove(Person person) {
        this.people.remove(person);
    }

    @Override
    public List<Person> getPersons() {
        return this.people;
    }
}
