package designPatterns.behavioralDesignPatterns.commandPattern;

/**
 * Created by ahiticas on
 * 10/20/2017.
 */


/*      Command Pattern -> we can encapsulate method invocation, it encapsulate a request as an object

        GOOD -> the object invoking the computation know nothing about the implementation.

        4 components: command, receiver, invoker and client

        1) Command: knows about receiver and invokes a method of the receiver. Values for parameters of the receiver method
                    are stored int the command!!

        2) Receiver: does the work itself.

        3) Invoker: knows how to execute a command, and optionally does bookkeeping about the command execution.
                    The invoker does know anything about a concrete command but knows only about command interface.
        4) Client: The client decides which commands to execute at which points. To execute a command, it passes the command object to invoker object.
 */

public class Main {

    public static void main(String[] args) {
        Switcher switcher = new Switcher();
        Light light = new Light();

        TurnOnCommand turnOnCommand = new TurnOnCommand(light);
        TurnOffCommand turnOffCommand = new TurnOffCommand(light);

        switcher.commands.add(turnOnCommand);
        switcher.commands.add(turnOffCommand);

        switcher.executeCommands();
    }
}
