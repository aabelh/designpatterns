package designPatterns.behavioralDesignPatterns.observerPattern;

/**
 * Created by ahiticas on
 * 10/19/2017.
 */

public interface Subject {
    public void addObserver(Observer o);
    public void removeObserver(Observer o);
    public void notifyAllObservers();
}
