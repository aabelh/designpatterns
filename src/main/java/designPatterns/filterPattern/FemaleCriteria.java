package designPatterns.filterPattern;

import java.util.ArrayList;
import java.util.List;

public class FemaleCriteria implements Criteria {
    @Override
    public List<Person> meetCriteria(List<Person> list) {
        List<Person> femaleCriteria = new ArrayList<Person>();

        for(Person p : list){
            if(p.getGender().equals(Person.Gender.FEMALE))
                femaleCriteria.add(p);
        }
        return femaleCriteria;
    }
}
