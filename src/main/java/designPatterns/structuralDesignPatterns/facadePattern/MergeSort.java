package designPatterns.structuralDesignPatterns.facadePattern;

import com.sun.org.apache.xpath.internal.SourceTree;

public class MergeSort implements Algorithm {
    @Override
    public void sort() {
        System.out.println("Using MergeSort");
    }
}
