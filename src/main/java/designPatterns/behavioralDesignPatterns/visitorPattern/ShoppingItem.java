package designPatterns.behavioralDesignPatterns.visitorPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public interface ShoppingItem {
    public double accpet(ShoppingCartVisitor visitor);
}
