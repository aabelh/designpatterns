package designPatterns.behavioralDesignPatterns.commandPattern;

/**
 * Created by ahiticas on
 * 10/20/2017.
 */

public class TurnOffCommand implements Command {

    private Light light;


    public TurnOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        this.light.turnOff();
    }
}
