package designPatterns.structuralDesignPatterns.decoratorPattern;

public interface Beverage {

    int getCost();
    String getDescription();
}
