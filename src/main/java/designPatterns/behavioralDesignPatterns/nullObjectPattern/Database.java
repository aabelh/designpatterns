package designPatterns.behavioralDesignPatterns.nullObjectPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class Database {
    private List<String> customerNames;


    public Database() {

        this.customerNames = new ArrayList<String>();
        customerNames.add("Daniel");
        customerNames.add("Adam");
        customerNames.add("Joe");
        customerNames.add("Michel");
    }

    public boolean existCustomer (String name) {
        for(String s: customerNames)
            if (s.equals(name))
                return true;

        return false;
    }
}
