package designPatterns.behavioralDesignPatterns.visitorPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class ShoppingCart implements ShoppingCartVisitor {


    @Override
    public double visit(Table table) {
        return table.getPrice();
    }

    @Override
    public double visit(Chair chair) {
        return chair.getPrice();
    }
}
