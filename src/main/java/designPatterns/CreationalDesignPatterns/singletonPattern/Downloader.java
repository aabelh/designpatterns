package designPatterns.CreationalDesignPatterns.singletonPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class Downloader {

    //eager version
    private static Downloader downloader = new Downloader();
    private static Downloader lazyDownloader;

    private Downloader() {
    }

    public static Downloader getInstance() {
        return Downloader.downloader;
    }

    public void startDownloading(){
        System.out.println("Start downloading data from the web");
    }


    //lazy version
    public static Downloader getLazyInstance() {

        if(lazyDownloader == null)
            lazyDownloader = new Downloader();
        return Downloader.lazyDownloader;
    }

}
