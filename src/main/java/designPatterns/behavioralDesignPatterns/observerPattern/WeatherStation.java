package designPatterns.behavioralDesignPatterns.observerPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahiticas on
 * 10/19/2017.
 */

public class WeatherStation implements Subject {

    private int pressure;
    private int temp;
    private int humidity;
    private List<Observer> observerList;

    public WeatherStation() {
        this.observerList = new ArrayList<>();
    }

    public void addObserver(Observer o) {
        this.observerList.add(o);
    }

    public void removeObserver(Observer o) {
        this.observerList.remove(o);
    }

    public void notifyAllObservers() {
        observerList.stream().forEach(observer -> observer.update(pressure, temp, humidity));
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
        notifyAllObservers();
    }

    public void setTemp(int temp) {
        this.temp = temp;
        notifyAllObservers();
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
        notifyAllObservers();
    }
}
