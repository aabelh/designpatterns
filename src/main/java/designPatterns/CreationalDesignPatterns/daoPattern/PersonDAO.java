package designPatterns.CreationalDesignPatterns.daoPattern;

import java.util.List;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public interface PersonDAO {

    void insert(Person person);

    void remove(Person person);

    List<Person> getPersons();

}
