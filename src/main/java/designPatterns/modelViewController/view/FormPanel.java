package designPatterns.modelViewController.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormPanel extends JPanel implements ActionListener {

    private JTextField nameField;
    private JTextField occupationField;
    private JButton okButton;
    private FormListener formListener;

    public FormPanel() {
       intiClasses();
       setClassDimensions();
       setupLayout();
    }

    private void intiClasses() {
        this.nameField = new JTextField(10);
        this.occupationField = new JTextField(10);
        this.okButton = new JButton("Ok");
        this.okButton.addActionListener(this);

    }

    private void setClassDimensions() {
        Dimension dim = getPreferredSize();
        dim.width = 300;
        setPreferredSize(dim);
        setMinimumSize(dim);
    }

    private void setupLayout() {
        setLayout(new GridBagLayout());

        GridBagConstraints gc = new GridBagConstraints();
        gc.gridx = 0;
        gc.gridy = 0;
        gc.weightx = 1;
        gc.weighty = 0.3;
        gc.fill = GridBagConstraints.NONE;
        gc.anchor = GridBagConstraints.LINE_END;

        add(new JLabel("Name: "),gc);
        gc.gridx++;
        gc.gridy = 0;
        gc.fill = GridBagConstraints.NONE;
        gc.anchor = GridBagConstraints.LINE_START;
        add(nameField, gc);

        //new row
        gc.gridy++;
        gc.gridx = 0;
        gc.weightx = 1;
        gc.weighty = 0.4;
        gc.fill = GridBagConstraints.NONE;
        gc.anchor = GridBagConstraints.LINE_END;

        add(new JLabel("Occupation: "), gc);

        gc.gridx++;
        gc.gridy = 1;
        gc.anchor = GridBagConstraints.LINE_START;
        add(occupationField, gc);

        //new row

        gc.gridy++;
        gc.gridx = 0;
        gc.weightx = 0;
        gc.weighty = 10;
        gc.fill = GridBagConstraints.NONE;
        gc.anchor = GridBagConstraints.FIRST_LINE_END;
        add(okButton, gc);

    }

    public void setFormListener(FormListener listener){
        this.formListener = listener;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(this.formListener != null){
            String persName = this.nameField.getText();
            String perOcup = this.occupationField.getText();
            this.formListener.okButtonClicked(persName, perOcup);
        }
    }
}
