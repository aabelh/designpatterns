package designPatterns.modelViewController;

import designPatterns.modelViewController.controller.Controller;
import designPatterns.modelViewController.view.MainFrame;

public class App {

    /**
     * MVC Pattern: we can separate the application with the help of this pattern
     *  Why is it good? if we want to add extra features it can be done very very easily !!
     *
     *      - Model: represents an object ot a class carrying data
     *          It can also have logic to update controller if its data changes
     *
     *      - Controller: acts on both model and view
     *          It controls the data flow into model object and
     *          updates the View whenever data changes
     *
     *      - View: represents the visualization of the data model contains
     *
     *
     *      MODEL <---> CONTROLLER <---> View
     */

    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();

        Controller controller = new Controller(mainFrame);
    }
}
