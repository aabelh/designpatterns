package designPatterns.behavioralDesignPatterns.observerPattern;

/**
 * Created by ahiticas on
 * 10/18/2017.
 */

    /*
            Observer Pattern

            Defines a one-to-many dependency -> if one object change state all of its dependents are notify automatically

             The Observer rely/dependent on the subject

             Why is it GOOD? ->> LOOSE COUPLING

                -> when two object are loosely coupled, they can interact but they have a little knowledge of each other.
                -> the only thing the subject knows about an observer is that it implements a certaininterface.
                -> we can add observers whenever we want: just have to implement the Observer interface
                -> we can independently reuse subjects or observers
                -> we can change the subject or observer independently

           So loosely coupled design is very good: we can build flexible systems that can handle change because the
           interdependency between objects are minimal.

          Very important principle in design -> USE LOOSELY COUPLED DESIGN BETWEEN OBJECTS THAT INTERACT.

     */

public class Main {
    public static void main(String[] args) {

        WeatherStation  station = new WeatherStation();
        WeatherObserver observer = new WeatherObserver(station);

        station.setHumidity(100);
        station.setPressure(50);
        station.setTemp(25);
    }
}
