package designPatterns.behavioralDesignPatterns.nullObjectPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class NullCustomer extends AbstractCustomer {


    public boolean isNull() {
        return true;
    }

    public String getCustomer() {
        return "No Customer with the given name exists in db...";
    }
}
