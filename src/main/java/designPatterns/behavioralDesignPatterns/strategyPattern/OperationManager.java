package designPatterns.behavioralDesignPatterns.strategyPattern;

/**
 * Created by ahiticas on
 * 10/18/2017.
 */

public class OperationManager implements Strategy {

    private Strategy strategy;

    public OperationManager(Strategy strategy) {
        this.strategy = strategy;
    }

    public void operation(int num1, int num2) {
        this.strategy.operation(num1, num2);
    }
}
