package designPatterns.behavioralDesignPatterns.strategyPattern;

/**
 * Created by ahiticas on
 * 10/18/2017.
 */

public interface Strategy {
    public void operation(int num1, int num2);
}
