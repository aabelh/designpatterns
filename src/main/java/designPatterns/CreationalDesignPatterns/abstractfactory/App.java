package designPatterns.CreationalDesignPatterns.abstractfactory;

import java.util.HashMap;

public class App {
    public static void main(String[] args) {
        Computer lenovoPc = ComputerFactory.getComputer(new LenovoPcFactory());
        Computer macBook = ComputerFactory.getComputer(new MacBookPcFactory());

        System.out.println(lenovoPc.getManufacturer());
        System.out.printf(macBook.getManufacturer());
    }
}
