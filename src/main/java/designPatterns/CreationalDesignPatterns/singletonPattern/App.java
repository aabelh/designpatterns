package designPatterns.CreationalDesignPatterns.singletonPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class App {


    /*
        Singleton Pattern -> restricts the instantiation of a class to one object
        This is useful when exactly one object is needed to coordinate actions across the system

        example: we need just single database instance.
     */

    public static void main(String[] args) {

       Downloader d = Downloader.getInstance();

       Downloader d2 = Downloader.getInstance();

       d.startDownloading();
       if (d == d2)
           System.out.println("they ar the same");
    }
}
