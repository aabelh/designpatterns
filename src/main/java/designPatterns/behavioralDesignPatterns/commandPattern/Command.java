package designPatterns.behavioralDesignPatterns.commandPattern;

/**
 * Created by ahiticas on
 * 10/20/2017.
 */

public interface Command {

    public void execute();
}
