package designPatterns.structuralDesignPatterns.facadePattern;

public class SortingManager {
    private Algorithm bubleSort;
    private Algorithm mergeSort;
    private Algorithm insertionSort;

    public SortingManager() {
        this.bubleSort = new BubleSort();
        this.mergeSort = new MergeSort();
        this.insertionSort = new InsertionSort();
    }

    public void doBubleSort(){
        this.bubleSort.sort();
    }

    public void doMergeSort(){
        this.mergeSort.sort();
    }

    public void doInsertionSort(){
        this.insertionSort.sort();
    }
}
