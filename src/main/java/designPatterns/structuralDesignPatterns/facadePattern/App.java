package designPatterns.structuralDesignPatterns.facadePattern;

public class App {

    /**
        * This pattern provide a unified interface to set of interfaces in system
        *
        *          It's defines a high level interface that makes the subsystem easier to use
        *
        */


    public static void main(String[] args) {
        SortingManager sortingManager = new SortingManager();

        sortingManager.doBubleSort();
        sortingManager.doMergeSort();
    }
}
