package designPatterns.CreationalDesignPatterns.abstractfactory;

public class LenovoPcFactory implements ComputerAbstractFactory {
    @Override
    public Computer create() {
        return new LenovoPc();
    }
}
