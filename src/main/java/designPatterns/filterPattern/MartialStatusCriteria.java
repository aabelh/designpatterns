package designPatterns.filterPattern;

import java.util.List;

public class MartialStatusCriteria implements Criteria {

    private Person.MaritalStatus maritalStatus;


    public MartialStatusCriteria(Person.MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }


    @Override
    public List<Person> meetCriteria(List<Person> list) {
        for(Person p : list){
            if(!p.getMaritalStatus().equals(maritalStatus)){
                list.remove(p);
            }
        }
        return list;
    }
}
