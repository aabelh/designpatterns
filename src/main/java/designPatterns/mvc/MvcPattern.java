package designPatterns.mvc;

public class MvcPattern {

    public static void main(String[] args) {

        Student model = retriveStudentFromDB();
        StudentView view = new StudentView();

        StudentController controller = new StudentController(model, view);

        controller.updateUi();
        controller.setStudentName("Nabel");
        controller.updateUi();

    }

    private static Student retriveStudentFromDB() {
        Student student = new Student();
        student.setName("Robert");
        student.setRollNo("10");
        return student;
    }
}
