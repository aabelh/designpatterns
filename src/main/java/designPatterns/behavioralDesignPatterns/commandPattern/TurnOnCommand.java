package designPatterns.behavioralDesignPatterns.commandPattern;

/**
 * Created by ahiticas on
 * 10/20/2017.
 */

public class TurnOnCommand implements Command {

    private Light light;


    public TurnOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        this.light.turnOn();
    }
}
