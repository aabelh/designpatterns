package designPatterns.CreationalDesignPatterns.factoryPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class ShortestPath implements Algorithm {
    @Override
    public void solve() {
        System.out.println("Solving the shortest path problem");
    }
}
