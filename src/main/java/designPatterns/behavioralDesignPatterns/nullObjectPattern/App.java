package designPatterns.behavioralDesignPatterns.nullObjectPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class App {

        /*
            Null Object Pattern

            -motivation: references may be null
            -it can be very elaborate to deal with null references
            -we have to check whether a reference is null or not -> NullPointerException if we call methods on null objects


            GOOD TO AVOID NULL REFERENCES For example: return empty ArrayList instead of null
            Or we have to use several if() checks whether the references is null
                For example: dealing with binary trees

                    class Node {
                    private int data;
                    private Node leftChild;
                    private Node rightChild;
                    ...
                    }

               Solution: the null object design pattern

               We create an abstract class specifying various operation to be done
               Concrete classes extending this class and a null object class providing
               do nothing implementation of this class
                    this approach can be used where we need to check null value.
        */


    public static void main(String[] args) {
        CustomerFactory customerFactory = new CustomerFactory();
        System.out.println(customerFactory.getCustomer("Daniel").getCustomer());
        System.out.println(customerFactory.getCustomer("Kev").getCustomer());
    }

}
