package designPatterns.behavioralDesignPatterns.visitorPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class App {

    /*Visitor pattern -> way of separating an algorithm from an object structure on which it operates

        A practical result of this separation is the ability to add new operations to existing object structure without
        modifying those structures
            -we can solve open-closed principle with this design pattern as well.

            IMPORTANT:  we can extract common operations into a unique class.
            For example: web shop app

     */

    public static void main(String[] args) {

        List<ShoppingItem> items = new ArrayList<>();
        items.add(new Table("desk", 20));
        items.add(new Chair("chairType1", 22));
        items.add(new Chair("ChairTyep2", 30));

        double sum = 0;
        ShoppingCartVisitor shoppintCart = new ShoppingCart();

        for (ShoppingItem item: items){
            sum = sum + item.accpet(shoppintCart);
        }

        System.out.println(sum);
    }
}
