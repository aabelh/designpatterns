package designPatterns.behavioralDesignPatterns.templatePattern;

/**
 * Created by ahiticas on
 * 10/31/2017.
 */

public class InsertionSrot extends Algorithm {

    private  int [] numbers;

    public InsertionSrot(int[] numbers) {
        this.numbers = numbers;
    }

    @Override
    public void initialize() {
        System.out.println("init insertion sort");
    }

    @Override
    public void sorting() {

        int tmp;
        int j;
        for (int i=1; i < this.numbers.length; i++) {
            tmp = this.numbers[i];
            j=i;

            while (j>0 && numbers[j-1] > tmp){
                numbers[j] = numbers[j-1];
                j=j-1;
            }
            numbers[j] = tmp;
        }

    }

    @Override
    public void printResult() {
        for (Integer in: numbers) {
            System.out.print(in + " ");
        }
    }
}
