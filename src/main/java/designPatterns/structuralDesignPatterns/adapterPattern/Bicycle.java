package designPatterns.structuralDesignPatterns.adapterPattern;

public class Bicycle {

    public void go(){
        System.out.println("going by bicycle");
    }
}
