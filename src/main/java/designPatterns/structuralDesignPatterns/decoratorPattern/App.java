package designPatterns.structuralDesignPatterns.decoratorPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class App {
    /*
        Important design principle - classes should be opened for extensions but closed for modification.

        For example: Observer Pattern we can add new Observers + extend Subject without adding code to Subject
        +DECORATOR pattern helps to make open closed principle come true

      -our goal is to allow classes to be easily extended with new behavior without modifying existing code

      -it is good -> FLEXIBLE! if te requirements are changing we do not have to rewrite the whole application

      -applying the open-closed principle everywhere can lead hard to understand code.

      -------------------
      Decorator pattern: attaches additional responsibilities to an object dynamically.

      Decorators provide a flexible alternative to subclassing for extending functionality

      For example: new LineNumberInputStream(new BufferedInputStream(new FileInputStream()));


     */

    public static void main(String[] args) {

        Beverage b = new Sugar(new Sugar(new Milk(new PlainBeverage())));

        System.out.println(b.getDescription());
        System.out.println(b.getCost());

    }
}
