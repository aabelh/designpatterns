package designPatterns.CreationalDesignPatterns.SingletonePattern2;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class App {


    /*
            Singleton Pattern - thread safe variant
            enum is synchronized by default
     */

    public static void main(String[] args) {
        SingletonClass.INSTANCE.setCounter(10);

        System.out.println(SingletonClass.INSTANCE.getCounter());

    }
}
