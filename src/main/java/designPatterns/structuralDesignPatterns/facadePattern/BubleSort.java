package designPatterns.structuralDesignPatterns.facadePattern;

public class BubleSort implements Algorithm {
    @Override
    public void sort() {
        System.out.println("Using bubleSort");
    }
}
