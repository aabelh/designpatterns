package designPatterns.behavioralDesignPatterns.templatePattern;

/**
 * Created by ahiticas on
 * 10/31/2017.
 */

public class Main {

    /*
        In Template pattern, an abstract class exposes defined way/template ti execute methods

        Its subclasses can override the method implementation as per need but the invocation is to be in the same way as
            defined by an abstract class

            abstract class Game() {

            abstract void initialize();
            abstract void startGame();
            abstract void finishGame();
            }
     */

    public static void main(String[] args) {

        int [] numbers = {1, 4, 2, -2, 0};

        Algorithm algorithm = new InsertionSrot(numbers);

        algorithm.sort();

    }
}
