package designPatterns.filterPattern;

import designPatterns.filterPattern.Person.Gender;

import java.util.ArrayList;
import java.util.List;

import static designPatterns.filterPattern.Person.*;

public class App {

    public static void main(String[] args) {

        List<Person> personList = new ArrayList<Person>();

        personList.add(new Person("Abel ", Gender.MALE, MaritalStatus.SINGLE, 23));
        personList.add(new Person("Mirela", Gender.FEMALE, MaritalStatus.SINGLE, 21));
        personList.add(new Person("Monica", Gender.FEMALE, MaritalStatus.SINGLE, 21));


        Criteria maleCriteria = new MaleCriteria();
        System.out.println("Males: ");
        printPersons(maleCriteria.meetCriteria(personList));

        Criteria genderCriteria = new GenderCriteria(Gender.MALE);
        System.out.println("GenderCriteria:");
        printPersons(genderCriteria.meetCriteria(personList));

        Criteria singleMaritalSratusCriteria = new MartialStatusCriteria(MaritalStatus.SINGLE);
        singleMaritalSratusCriteria.meetCriteria(personList);

        System.out.println("OR Criteria");
        OrCriteria orCriteria = new OrCriteria(singleMaritalSratusCriteria, genderCriteria);
        printPersons(orCriteria.meetCriteria(personList));




    }


    public static void printPersons(List<Person> persons){

        for (Person person : persons) {
            System.out.println(person.toString());
        }
    }
}
