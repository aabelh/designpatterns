package designPatterns.CreationalDesignPatterns.builderPattern;

/**
 * Created by ahiticas on
 * 11/28/2017.
 */

public class App {

        /*
            WHY to prefer builder pattern ?

            -there would be lots of parameters in the constructor - hard to flow
            Hard not confuse parameters.
                -setters are the same, not the best solution

            -not extensible: when we have to add one more optional parameters, we have to create a new constructor
                Telescoping constructors appear.
                    With a builder -> we just add another method.
            -immutable property: we should consider parallel execution
                The best is to use objects that can not be modified after they have been created "immutable objects"
            -so there will not be any problems because of concurrent updates by multiple threads

         */

    public static void main(String[] args) {

        Person person = new  Person.Builder("kevin", "kevin@gmail.com")
                .setAge(10)
                .setUniversity("unive1")
                .setAddress("address 1")
                .build();

        System.out.println(person);

    }

}
